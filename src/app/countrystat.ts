export interface CountryStat {
  name: string;
  countryCodeThree: string;
  year: number;
  population: number;
  gdp: number;
}
