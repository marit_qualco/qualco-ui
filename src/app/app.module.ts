import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CountriesComponent } from './countries/countries.component';
import { CountriesSpokenLanguagesComponent } from './countries-spoken-languages/countries-spoken-languages.component';
import { CountriesStatisticsComponent } from './countries-statistics/countries-statistics.component';
import { CountriesContinentsComponent } from './countries-continents/countries-continents.component';
import {HttpClientModule} from "@angular/common/http";
import {ApiService} from "./api.service";

@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    CountriesSpokenLanguagesComponent,
    CountriesStatisticsComponent,
    CountriesContinentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
