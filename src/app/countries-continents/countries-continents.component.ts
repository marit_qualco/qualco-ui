import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Language} from "../language";
import {ApiService} from "../api.service";
import {ActivatedRoute} from "@angular/router";
import {ContinentCountry} from "../continentCountry";

@Component({
  selector: 'app-countries-continents',
  templateUrl: './countries-continents.component.html',
  styleUrls: ['./countries-continents.component.css']
})
export class CountriesContinentsComponent implements OnInit {

  continentCountries: Observable<ContinentCountry[]>;

  constructor(private apiService: ApiService) {
    this.continentCountries = this.apiService.getContinentCountries();
  }

  ngOnInit(): void {
  }

}

