import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Country} from "../country";
import {ApiService} from "../api.service";
import {Language} from "../language";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-countries-spoken-languages',
  templateUrl: './countries-spoken-languages.component.html',
  styleUrls: ['./countries-spoken-languages.component.css']
})
export class CountriesSpokenLanguagesComponent implements OnInit {

  languages: Observable<Language[]>;

  constructor(private apiService: ApiService, private route:ActivatedRoute) {
    this.languages = this.apiService.getLanguages(Number(this.route.snapshot.paramMap.get('id')));
  }

  ngOnInit(): void {
  }

}
