import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountriesSpokenLanguagesComponent } from './countries-spoken-languages.component';

describe('CountriesSpokenLanguagesComponent', () => {
  let component: CountriesSpokenLanguagesComponent;
  let fixture: ComponentFixture<CountriesSpokenLanguagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountriesSpokenLanguagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountriesSpokenLanguagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
