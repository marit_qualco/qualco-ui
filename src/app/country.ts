export interface Country {
  id: number;
  name: string;
  area: number;
  countryCodeTwo: string;
}
