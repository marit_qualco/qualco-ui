import { Injectable } from '@angular/core';
import {Country} from "./country";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Language} from "./language";
import {ContinentCountry} from "./continentCountry";
import {CountryStat} from "./countrystat";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  countries: Country[] = [];

  languages: Language[] = [];

  continentCountries: ContinentCountry[] = [];

  countriesStats: CountryStat[] = [];

  private countriesUrl = 'http://localhost:8080/countries'

  private languagesUrl = 'http://localhost:8080/languages/'

  private continentCountriesUrl = 'http://localhost:8080/continents'

  private countriesStatsUrl = 'http://localhost:8080/countriesstats'

  constructor(private http: HttpClient) { }

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(this.countriesUrl);
  }

  getLanguages(id: number): Observable<Language[]> {
    return this.http.get<Language[]>(this.languagesUrl+id);
  }

  //TODO add filters
  getContinentCountries(): Observable<ContinentCountry[]> {
    return this.http.get<ContinentCountry[]>(this.continentCountriesUrl);
  }

  getCountriesStats(): Observable<CountryStat[]> {
    return this.http.get<CountryStat[]>(this.countriesStatsUrl);
  }

}
