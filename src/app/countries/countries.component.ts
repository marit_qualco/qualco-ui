import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {Country} from "../country";
import {Observable} from "rxjs";

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {

  countries: Observable<Country[]>;


  constructor(private apiService: ApiService) {
    this.countries = this.apiService.getCountries();
  }

  ngOnInit(): void {
  }

}
