import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CountriesComponent} from "./countries/countries.component";
import {CountriesContinentsComponent} from "./countries-continents/countries-continents.component";
import {CountriesSpokenLanguagesComponent} from "./countries-spoken-languages/countries-spoken-languages.component";
import {CountriesStatisticsComponent} from "./countries-statistics/countries-statistics.component";

const routes: Routes = [
  { path: 'countries', component: CountriesComponent },
  { path: 'continents', component: CountriesContinentsComponent },
  { path: 'languages/:id', component: CountriesSpokenLanguagesComponent },
  { path: 'countriesstats', component: CountriesStatisticsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
