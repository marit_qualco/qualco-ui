import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Language} from "../language";
import {ApiService} from "../api.service";
import {ActivatedRoute} from "@angular/router";
import {CountryStat} from "../countrystat";

@Component({
  selector: 'app-countries-statistics',
  templateUrl: './countries-statistics.component.html',
  styleUrls: ['./countries-statistics.component.css']
})
export class CountriesStatisticsComponent implements OnInit {

  countriesStats:  Observable<CountryStat[]>;

  constructor(private apiService: ApiService) {
    this.countriesStats = this.apiService.getCountriesStats();
  }

  ngOnInit(): void {
  }

}
